//Card Filter

const urgencySelect = document.querySelector("#urgencySelect");
urgencySelect.addEventListener("change", cardsFilter);

function cardsFilter(item) {
  let filterValue = item.target.value;

  let urgencyStatus = document.querySelectorAll(".URGENCY");

  urgencyStatus.forEach((e) => {
    let currentInnerText = e.innerText.split(" ")[1];

    const deleteWrapper = e.closest(".card_wrapper");

    if (filterValue !== currentInnerText) {
      deleteWrapper.style.display = "none";
    }
    if (filterValue === currentInnerText || filterValue === "all") {
      deleteWrapper.style.display = "block";
    }
  });
}

//Select Search
const statusSelect = document.querySelector("#statusSelect");
statusSelect.addEventListener("change", statusFilter);

function statusFilter(item) {
  let filterValue = item.target.value;

  let statusStatus = document.querySelectorAll(".STATUS");

  statusStatus.forEach((e) => {
    let currentInnerText = e.innerText.split(" ")[1];

    const deleteWrapper = e.closest(".card_wrapper");

    if (filterValue !== currentInnerText) {
      deleteWrapper.style.display = "none";
    }
    if (filterValue === currentInnerText || filterValue === "all") {
      deleteWrapper.style.display = "block";
    }
  });
}

///Input Search

const inputSearch = document.querySelector("#search_filter");
let accum = "";

inputSearch.addEventListener("input", (e) => {
  if (e.data === null) {
    accum = accum.slice(0, inputSearch.value.length);
  } else {
    accum += e.data;
  }

  let allCards = document.querySelectorAll(".card_wrapper");

  allCards.forEach((oneCard) => {
    const description = oneCard.querySelector(".DESCRIPTION");
    const nameCard = oneCard.querySelector(".fullName");
    const nameDoctor = oneCard.querySelector(".doctor");
    const inputSearchValue = inputSearch.value.toLowerCase();

    function checkText(element) {
      return element.innerText.toLowerCase().includes(inputSearchValue);
    }

    const currentDescription = checkText(description);
    const currentName = checkText(nameCard);
    const currentDoctor = checkText(nameDoctor);

    if (
      currentDescription === true ||
      currentName === true ||
      currentDoctor === true
    ) {
      oneCard.style.display = "block";
    } else {
      oneCard.style.display = "none";
    }
  });
});
