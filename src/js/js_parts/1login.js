const logInButton = document.querySelector(".log_in_button");
const logInForm = document.querySelector(".login_form");
const content = document.querySelector(".main_conteiner");
const passwordInput = document.getElementById("exampleInputPassword1");
const emailInput = document.getElementById("exampleInputEmail1");
const mistake = document.querySelector(".email_wrong");
const cardWrapper = document.querySelector(".cards_wrapper");
const main = document.querySelector(".main");
const closeLogin = document.getElementById("close_login");
const submitButton = document.querySelector(".login_submit");
const createAppoint = document.getElementById("new_appointment_button");
let token = "";
let objects;
logInButton.addEventListener("click", (event) => {
  event.preventDefault();
  logInForm.style.display = "flex";
  main.classList.add("login_bg");
});

async function authorisation(email, password) {
  if (email !== "cooluser@gmail.com" || password !== "SuperPuper123") {
    mistake.style.opacity = "1";
  } else {
    logInForm.style.display = "none";
    cardWrapper.style.display = "block";
    content.style.display = "none";
    main.classList.remove("login_bg");
    logInButton.style.display = "none";
    createAppoint.style.display = "block";

    await fetch("https://ajax.test-danit.com/api/v2/cards/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ email: "your@email.com", password: "password" }),
    })
      .then((response) => response.text())
      .then((token) => {
        return token;
      })
      .then((result) => (token = result));
    const cardAll = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    };
    sendRequest("https://ajax.test-danit.com/api/v2/cards", "GET", cardAll)
      .then((response) => response)
      .then((objects) => {
        objects.map((item) => {
          const modalFromServer = new Modal();
          modalFromServer.checkingForCards();
          modalFromServer.createCard(item, item.id, "DELETE");
        });
      });
  }
}

logInForm.addEventListener("submit", (event) => {
  const email = event.target[0].value;
  const password = event.target[1].value;
  authorisation(email, password);
  event.preventDefault();
});

function handlePasswordInput() {
  mistake.style.opacity = "0";
}
passwordInput.addEventListener("focus", () => handlePasswordInput());
emailInput.addEventListener("focus", () => handlePasswordInput());

document.body.addEventListener("click", (event) => {
  if (event.target.closest("#close_login")) {
    logInForm.style.display = "none";
    main.classList.remove("login_bg");
  }
});
