class VisitTherapist extends Modal {
  constructor(lastVisit, doctor) {
    super(doctor);
    this.lastVisit = lastVisit;
  }

  test(likar, ageDev = "") {
    const doctorWrapper = document.querySelector(".specialDoctorWrapper");
    if (likar === "therapist") {
      doctorWrapper.innerHTML = "";
      doctorWrapper.insertAdjacentHTML(
        "beforeend",
        `
              <div class="form__content__item item">
                  <label for="age">Age</label>
                  <input required value="${ageDev}" class="item__input"
                  id="age"
                  name="age"
                  type="text"
                  tabindex="7"
                  />
              </div>
              <button type="submit" class="submit__card" >Send</button>
              `
      );
    }
  }
}
