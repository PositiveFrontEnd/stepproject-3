let dataUser;
document.body.addEventListener("click", async (event) => {
  if (event.target.dataset.button === "change") {
    event.preventDefault();

    const cardId = event.target.closest(".card_wrapper").id;
    createAppoint.disable = true;

    const sendRequestCard = {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    sendRequest(`${API}/${cardId}`, "GET", sendRequestCard).then((data) => {
      dataUser = data;
      const {
        age,
        description,
        doctor,
        fullName,
        urgency,
        visit,
        data: date,
        disease,
        massa,
        pressure,
        statusCard,
      } = data;

      modal.render(description, doctor, fullName, urgency, visit, statusCard);
      modal.closeCardForm();
      const visitDentist = new VisitDentist();
      visitDentist.test(data.doctor, date);

      const visitCardiologist = new VisitCardiologist();
      visitCardiologist.test(data.doctor, pressure, massa, disease, age);

      const visitTherapist = new VisitTherapist();
      visitTherapist.test(data.doctor, age);
      const doctorSelect = document.getElementById("doctors__select");
      doctorSelect.disabled = true;
      const statusSelect = document.getElementById("status__card");
      statusSelect.disabled = false;
      modal.submit("PUT", data.id);
    });
  }
});
