const deleteAllCards = document.querySelector(".delete-button");
deleteAllCards.addEventListener("click", () => {
  const cardAll = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
  };
  sendRequest("https://ajax.test-danit.com/api/v2/cards", "GET", cardAll)
    .then((response) => response)
    .then((response) => {
      const arr = response;
      arr.forEach((element) => {
        const cardWrapper = document.getElementById(`${element.id}`);
        console.log(element);
        const createCardRequest = {
          method: "DELETE",
          headers: { Authorization: `Bearer ${token}` },
        };
        sendRequest(`${API}/${element.id}`, "DELETE", createCardRequest)
          .then((response) => {
            if (response.status === 200) {
              cardWrapper.remove();
              modal.checkingForCards();
            } else {
              throw new Error("Помилка видалення картки");
            }
          })
          .catch((error) => {
            console.log(error.message);
          });
      });
    })
    .catch((error) => {
      console.log(error.message);
    });
});
