const API = "https://ajax.test-danit.com/api/v2/cards";

const orderCards = document.querySelector(".order__cards");
const cardsWrapper = document.createElement("div");
const modalWrapper = document.querySelector(".modal_cards");
cardsWrapper.classList.add("cards_conteiner");
let statusDelete;

class Modal {
  constructor(name, id, objective, description, urgency, doctor) {
    this.name = name;
    this.id = id;
    this.objective = objective;
    this.description = description;
    this.urgency = urgency;
    this.doctor = doctor;
  }
  checkingForCards() {
    const noCards = document.querySelector(".no__cards");
    if (cardsWrapper.innerHTML !== "") {
      noCards.style.display = "none";
    } else {
      noCards.style.display = "block";
    }
  }
  render(
    descriptionDev = "",
    doctorDev = "Choose a doctor",
    fullNameDev = "",
    urgencyDev = "normal",
    visitDev = "",
    statusCardDev = "Open"
  ) {
    modalWrapper.insertAdjacentHTML(
      "beforeend",
      `
            <section class="cards">
            <form action="" class="cards__form form "> 
            <p class="cards__closed" jsname="itVqKe"><svg focusable="false" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"></path></svg></p>
        <p>Fill out the patient card</p>
        <select  class="cards__select" name="doctor" id="doctors__select">
            <option value="${doctorDev}" selected disabled>${doctorDev}</option>
            <option value="cardiologist">Cardiologist</option>
            <option value="dentist">Dentist</option>
            <option value="therapist">Therapist</option>
        </select>

    <div class="form__content__item item">
        <label for="fullName">Full name</label>
        <input required value="${fullNameDev}" class="item__input"
        id="fullName"
        name="fullName"
        type="text"
        tabindex="3"
        />
    </div>
            
        <div class="urgency">
            <select class="cards__select" name="urgency">
                <option value="${urgencyDev}" selected disabled>Urgency</option>
                <option value="high">High</option>
                <option value="normal">Normal</option>
                <option value="low">Low</option>
            </select>
        </div>
        <div class="status">
            <select class="cards__select" name="status" id='status__card' disabled>
                <option value="${statusCardDev}" selected disabled>${statusCardDev}</option>
                <option selected value="open">Open</option>
                <option value="done">Done</option>
            </select>
        </div>

    <div class="form__content">
      <div class="form__content__item item">
        <label for="visit">Purpose of visit</label>
        <input required value="${visitDev}" class="item__input"
          id="visit"
          name="visit"
          type="text"
          tabindex="1"
        />
      </div>
      <div class="form__content__item item">
        <label for="description">Description</label>
        <input required value="${descriptionDev}" class="item__input"
          id="description"
          name="description"
          type="text"
          tabindex="2"
        />
      </div>

      <div class="specialDoctorWrapper">
      </div>
     

    </div>
     </form>
    </div></section>
        `
    );
    main.classList.add("login_bg");
  }
  chooseDoctor() {
    const dropDownDoctor = document.querySelector("#doctors__select");
    dropDownDoctor.addEventListener("change", (event) => {
      this.doctor = event.target.value;

      const visitDentist = new VisitDentist();
      visitDentist.test(this.doctor);

      const visitCardiologist = new VisitCardiologist();
      visitCardiologist.test(this.doctor);

      const visitTherapist = new VisitTherapist();
      visitTherapist.test(this.doctor);
    });
    this.submit("POST");
  }

  closeCardForm() {
    const form = document.querySelector(".cards");
    const closed = document.querySelector(".cards__closed");
    closed.addEventListener("click", () => {
      main.classList.remove("login_bg");
      modalWrapper.innerHTML = "";
    });
    form.addEventListener("click", (event) => {
      event.stopPropagation();
    });
    main.addEventListener("click", () => {
      main.classList.remove("login_bg");
      modalWrapper.innerHTML = "";
    });
  }
  async submit(method, idCard) {
    const form = document.querySelector(".cards__form");
    form.addEventListener("submit", (event) => {
      event.preventDefault();
      const inputElements = event.target.querySelectorAll("[name]");
      const valuesObject = Array.from(inputElements).reduce(
        (accumulator, input) => {
          accumulator[input.name] = input.value;
          return accumulator;
        },
        {}
      );
      main.classList.remove("login_bg");
      modalWrapper.innerHTML = "";
      if (method === "POST") {
        const createCardRequest = {
          method: `${method}`,
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },
          body: JSON.stringify(valuesObject),
        };

        sendRequest(API, "POST", createCardRequest)
          .then((data) => {
            const { id } = data;
            this.createCard(valuesObject, id, "DELETE");
            this.checkingForCards();
          })
          .catch((error) => {
            console.error("Помилка:", error);
          });
      } else if (method === "PUT") {
        const sendRequestCards = {
          method: `${method}`,
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },
          body: JSON.stringify(valuesObject),
        };
        sendRequest(`${API}/${idCard}`, "PUT", sendRequestCards)
          .then((response) => response)

          .catch((error) => {
            console.error("Помилка: " + error);
          });

        const cardWrapper = document.getElementById(`${idCard}`);
        const moInfo = cardWrapper.querySelector(".another_info");
        for (let item in valuesObject) {
          if (item === "fullName") {
            cardWrapper.querySelector(".fullName").textContent =
              valuesObject[item];
          } else if (item === "doctor") {
            cardWrapper.querySelector(".doctor").textContent =
              valuesObject[item][0].toUpperCase() + valuesObject[item].slice(1);
          } else {
            const info = moInfo.querySelector(`.${item.toUpperCase()}`);
            info.textContent = `${item.toUpperCase()}: ${valuesObject[item]}`;
          }
        }
      }
    });
  }

  createCard(data, id, method) {
    const cardWrapper = document.createElement("div");
    cardWrapper.classList.add("card_wrapper");
    cardWrapper.setAttribute("id", id);

    const cardTitle = document.createElement("h3");
    cardTitle.classList.add("fullName");
    const cardDoctor = document.createElement("h4");
    cardDoctor.classList.add("doctor");

    const anotherInfoWrap = document.createElement("div");
    anotherInfoWrap.classList.add("another_info");

    const buttonsWrap = document.createElement("div");
    buttonsWrap.classList.add("button_wrapper");
    const showMore = document.createElement("p");
    showMore.innerText = "Show more...";
    showMore.classList.add("show_more");
    showMore.addEventListener("click", () => {
      showMore.style.display = "none";
      anotherInfoWrap.style.display = "block";
    });

    const changeButton = document.createElement("button");
    changeButton.innerText = "Edit";
    changeButton.classList.add("btn");
    changeButton.classList.add("btn-primary");
    changeButton.setAttribute("data-button", "change");

    const removeCard = document.createElement("button");
    removeCard.innerText = "Remove";
    removeCard.classList.add("btn");
    removeCard.classList.add("btn-primary");
    removeCard.setAttribute("data-button", "remove");
    removeCard.classList.add("remove__button");

    cardsWrapper.append(cardWrapper);
    cardWrapper.append(
      cardTitle,
      cardDoctor,
      showMore,
      anotherInfoWrap,
      buttonsWrap
    );
    buttonsWrap.append(changeButton, removeCard);
    for (let item in data) {
      if (item === "fullName") {
        cardTitle.innerText = data[item];
      } else if (item === "doctor") {
        cardDoctor.innerText =
          data[item][0].toUpperCase() + data[item].slice(1);
      } else {
        const info = document.createElement("p");
        info.innerText = `${item.toUpperCase()}: ${data[item]}`;
        info.classList.add(`${item.toUpperCase()}`);
        anotherInfoWrap.append(info);
      }
    }
    orderCards.append(cardsWrapper);

    removeCard.addEventListener("click", (event) => {
      const deleteCardFromHtml = event.target.closest(".card_wrapper");
      const createCardRequest = {
        method: `${method}`,
        headers: { Authorization: `Bearer ${token}` },
      };
      sendRequest(
        `${API}/${deleteCardFromHtml.id}`,
        "DELETE",
        createCardRequest
      )
        .then((response) => {
          if (response.status === 200) {
            cardWrapper.remove();
            this.checkingForCards();
          } else {
            throw new Error("Помилка видалення картки");
          }
        })
        .catch((error) => {
          console.log(error.message);
        });
    });
  }
}
const modal = new Modal();
createAppoint.addEventListener("click", () => {
  modal.render();
  modal.chooseDoctor();
  modal.closeCardForm();
});
