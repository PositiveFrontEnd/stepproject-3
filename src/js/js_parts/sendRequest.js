function sendRequest(url, method = "GET", options) {
  return fetch(url, { method: method, ...options }).then((response) => {
    if (response.ok) {
      if (method === "DELETE") {
        return response;
      }
      return response.json();
    }
  });
}
