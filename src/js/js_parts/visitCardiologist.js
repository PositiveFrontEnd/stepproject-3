class VisitCardiologist extends Modal {
  constructor(lastVisit, doctor) {
    super(doctor);
    this.lastVisit = lastVisit;
  }

  test(likar, pressureDev = "", massaDev = "", diseaseDev = "", ageDev = "") {
    const doctorWrapper = document.querySelector(".specialDoctorWrapper");
    if (likar === "cardiologist") {
      doctorWrapper.innerHTML = "";
      doctorWrapper.insertAdjacentHTML(
        "beforeend",
        `
                      <div class="form__content__item item">
                          <label for="pressure">Normal pressure</label>
                          <input required value="${pressureDev}" class="item__input"
                          id="pressure"
                          name="pressure"
                          type="text"
                          tabindex="4"
                          />
                      </div>
                      <div class="form__content__item item">
                          <label for="massa">Body mass index</label>
                          <input required value="${massaDev}" class="item__input"
                          id="massa"
                          name="massa"
                          type="text"
                          tabindex="5"
                          />
                      </div>
                      <div class="form__content__item item">
                          <label for="disease">Transferred cardiovascular diseases</label>
                          <input required value="${diseaseDev}" class="item__input"
                          id="disease"
                          name="disease"
                          type="text"
                          tabindex="6"
                          />
                      </div>
                      <div class="form__content__item item">
                          <label for="age">Age</label>
                          <input required value="${ageDev}" class="item__input"
                          id="age"
                          name="age"
                          type="text"
                          tabindex="7"
                          />
                  </div>
                  <button type="submit" class="submit__card" >Send</button>
              `
      );
    }
  }
}
