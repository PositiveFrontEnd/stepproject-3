class VisitDentist extends Modal {
  constructor(lastVisit, doctor) {
    super(doctor);
    this.lastVisit = lastVisit;
  }

  test(likar, dataDev = "") {
    const doctorWrapper = document.querySelector(".specialDoctorWrapper");
    if (likar === "dentist") {
      doctorWrapper.innerHTML = "";
      doctorWrapper.insertAdjacentHTML(
        "beforeend",
        `
              <div class="form__content__item item">
                  <label for="data">Date of last visit</label>
                  <input required value="${dataDev}" class="item__input"
                  id="data"
                  name="data"
                  type="text"
                  tabindex="8"
                  />
              </div>
  
              <button type="submit" class="submit__card" >Send</button>
  
              `
      );
    }
  }
}
